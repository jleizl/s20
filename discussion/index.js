// console.log("Hello World");


/*
    While loop - takes a single condition. If the condition is true it will run the code.

        Syntax:
            while(condition) {
                statement
            }

/

let count = 5

while(count !== 0) {
    console.log("While: " + count);
    count--;
}

let num = 0

while(num <= 5){
    console.log("While: " + num);
    num++
}

/MINI ACTIVITY

    THE while loop should only displaye even number/ incriment by 2.

    let numA = 0 
        while (numA = 30) {
            console.log("While: " +num)
            numA ++2
        }


*/

// let number = NUmber(prompt("Give me a number"));

// do {
// 	console.log("Do while: " + number);
// 	number++
// } while(number < 10)


// let number = NUmber(prompt("Give me another number"));

// do {
// 	console.log("Do while: " + number);
// 	number--
// } while(number > 10)

/*
for loop
	- more flexible that while loop and do-while loop

	- part:
		-initial value: tracks the progress of the loop.
		-condition: if true it will run the code; if false it will stop the iteration/ code 
		-iteration: 

*/


for (let count = 0; count <= 20; count++) {
    console.log("For loop count: " + count);
}

let myString = "jay leizl reyes"
console.log(myString.length);

console.log(" ");
console.log(myString[0]);
console.log(myString[10]);

console.log(" ");

// 
for (let x = 0; x < myString.length; x++) {
    console.log(myString[x])
}

let myName = "JAY LEIZL";

for (let n = 0; n < myName.length; n++) {
    if (
        myName[n].toLowerCase() == "a" ||
        myName[n].toLowerCase() == "e" ||
        myName[n].toLowerCase() == "i" ||
        myName[n].toLowerCase() == "o" ||
        myName[n].toLowerCase() == "u"
    ) {
    	console.log("Vowel")

    } else {
    	console.log(myName[n].toLowerCase())
    }
}

console.log(" ");

let word = "extravagant"
let consonants = " ";
for (let c = 0; c < word.length; c++){
	if(
		word[c].toLowerCase() == "a" ||
		word[c].toLowerCase() == "e" ||
		word[c].toLowerCase() == "i" ||
		word[c].toLowerCase() == "o" ||
		word[c].toLowerCase() == "u" 

	) {
		continue;

	} else {
		 
		consonants += word[c];
	}
} 

console.log(consonants)

// Continue and Break Statement

/*
	"continue" statement allows the code to go the 
*/

console.log(" ");

for (let count = 0; count < 20; count++) {

	if (count % 5 === 0) {
		console.log("Div by 5")
		continue;
	}
	console.log("continue and break: " + count)

	if (count > 10) {
		break;
	}
}