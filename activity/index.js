// S 20 - Javascript Repetition Control Structures

// ACTIVITY


/*
PART 1: 

	- Create a variable number that will store the value of the number provided by the user via the prompt.

	- Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	- Create a condition that if the current value is less than or equal to 50, stop the loop.

	- Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	- Create another condition that if the current value is divisible by 5, print the number.

*/

	// CODE HERE:

let num = Number(prompt("Provide a number:"));
	console.log("The number you provided is " + num);
	for (let n = num; n >= 0; n--) {
		if (n <= 50){
			console.log("Number is less than or equal to 50. Stop the loop.");
			break;
		}
		
		if (n % 10 === 0) {
			console.log("The number is divisible by 10. Skipping the number.");
			continue;
		}

		if (n % 5 === 0) {
			console.log(n);
			continue
		}
	}

/*
PART 2:

	- Create a varaible that will contain the string "supercalifragilisticexpialidocious"

	- Create another varaible that will store the consonants from the string.

	- Create a for loop that will iterate through the individual letters of the string based on it's length.

	- Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	- Create an else statement that will add the consonants to the second variable.


*/

	// CODE HERE:

	let word = "supercalifragilisticexpialidocious";
let consonants = " ";
console.log(word);
for (let c = 0; c < word.length; c++){
	if(
		word[c].toLowerCase() == "a" ||
		word[c].toLowerCase() == "e" ||
		word[c].toLowerCase() == "i" ||
		word[c].toLowerCase() == "o" ||
		word[c].toLowerCase() == "u" 

	) {
		continue;

	} else {
		 
		consonants += word[c];
	}
} 

console.log(consonants);